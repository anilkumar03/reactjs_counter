import {Component} from 'react'
import './index.css'
class Counter extends Component{
    state = {count:0}
    increment = ()=>{
        return this.setState((prevState)=>({count : prevState.count + 1})
        )
    }

    decrement = ()=>{
        return this.setState((prevState)=>({count : prevState.count - 1})
        )
    }

    reset = ()=>{
        return this.setState((prevState)=>({count : 0})
        )
    }

    render(){
        const {count} = this.state
        return(
            <div className='main'>
            <div className='counter-card'>
                <p className='count'>{count}</p>
                <div className='btn-container'>
                    <button className='button' onClick={this.increment}>Increment</button>
                    <button className='reset-btn' onClick={this.reset}>Reset</button>
                    <button className='button' onClick={this.decrement}>Decrement</button>
                </div>
            </div>
        </div>
        )
    }
}

export default Counter